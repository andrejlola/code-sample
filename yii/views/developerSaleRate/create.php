<?php
/* @var $this DeveloperSaleRateController */
/* @var $model DeveloperSaleRate */

$this->breadcrumbs = [
    'Показатели рейтов разработчиков' => ['index'],
    'Создать',
];
?>

<h1>Создать</h1>

<?php $this->renderPartial('_form', [
    'model' => $model,
]); ?>
