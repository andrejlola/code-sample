<?php

class TimePlanningController extends Controller
{
    public function actionIndex()
    {
        $year = Yii::app()->request->getParam('year');
        $month = Yii::app()->request->getParam('month');
        $employee = Yii::app()->request->getParam('employee');
        $data = [];
        if (($date = DateTime::createFromFormat('Y-m-d', $year . '-' . $month . '-01')) && ($employee > -1)) {
            $manager = LManager::get(Yii::app()->user->id);
            $data = $manager->getPlanTable($date, $employee);
        }
        $this->render('index', [
            'developers' => $data,
            'year' => $year,
            'month' => $month,
            'employee' => $employee,
        ]);
    }

    public function actionSetPlan()
    {
        $request = Yii::app()->request;
        $year = $request->getParam('year');
        $weekOfYear = $request->getParam('weekOfYear');
        $projectId = $request->getParam('projectId');
        $projectType = $request->getParam('projectType');
        $developerId = $request->getParam('developerId');
        $time = $request->getParam('time');
        $plan = LTimePlanning::setPlan($year, $weekOfYear, $projectId, $projectType, $developerId, $time);
        if ($plan->hasErrors()) {
            $success = false;
            $message = implode(', ', AppHelper::getErrorsList($plan->getErrors()));
        } else {
            $success = true;
            $message = 'Успешно';
        }
        $response = [
            'success' => $success,
            'message' => $message
        ];
        echo json_encode($response);
    }

    public function actionSetActuallyWorkedOutTime()
    {
        $request = Yii::app()->request;
        $date = $request->getParam('date');
        $developerId = $request->getParam('developerId');
        $time = $request->getParam('time');
        $actuallyWorkedOutTime = LTimePlanning::setActuallyWorkedOutTime($date, $developerId, $time);
        if ($actuallyWorkedOutTime->hasErrors()) {
            $success = false;
            $message = implode(', ', AppHelper::getErrorsList($actuallyWorkedOutTime->getErrors()));
        } else {
            $success = true;
            $message = 'Успешно';
        }
        $response = [
            'success' => $success,
            'message' => $message
        ];
        echo json_encode($response);
    }
}
