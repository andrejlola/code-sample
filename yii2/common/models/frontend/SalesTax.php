<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%SALES_TAX}}".
 *
 * @property string $ref_type
 * @property string $ref_id
 * @property string $tax_id
 * @property string $tax_name
 * @property double $tax_percent
 * @property string $date
 */
class SalesTax extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%SALES_TAX}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'ref_type',
                    'ref_id',
                    'tax_id'
                ],
                'required'
            ],
            [
                ['tax_percent'],
                'number'
            ],
            [
                ['date'],
                'safe'
            ],
            [
                ['ref_type'],
                'string',
                'max' => 10
            ],
            [
                [
                    'ref_id',
                    'tax_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['tax_name'],
                'string',
                'max' => 60
            ],
            [
                [
                    'ref_type',
                    'ref_id',
                    'tax_id'
                ],
                'unique',
                'targetAttribute' => [
                    'ref_type',
                    'ref_id',
                    'tax_id'
                ],
                'message' => 'The combination of Ref Type, Ref ID and Tax ID has already been taken.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ref_type' => 'Ref Type',
            'ref_id' => 'Ref ID',
            'tax_id' => 'Tax ID',
            'tax_name' => 'Tax Name',
            'tax_percent' => 'Tax Percent',
            'date' => 'Date',
        ];
    }
}
