<?php

namespace common\models\frontend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AutoMileageSearch represents the model behind the search form about `common\models\frontend\AutoMileage`.
 */
class InvoiceSearch extends Invoice
{
    public $dateFrom;
    public $dateTo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'dateFrom',
                ],
                'default',
                'value' => (new \DateTime())->sub(new \DateInterval('P1M'))
                    ->format('Y-m-d')
            ],
            [
                [
                    'dateTo',
                ],
                'default',
                'value' => (new \DateTime())->add(new \DateInterval('P1D'))
                    ->format('Y-m-d')
            ],

            [
                [
                    'date',
                    'inv_number',
                    'inv_date',
                    'due_date',
                    'source_id',
                    'po_number',
                    'terms',
                    'status',
                    'cust_terms',
                    'bill_addr',
                    'ship_addr',
                    'tax_rate',
                    'status',
                    'notes',

                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->dateFrom = $this->toStorageDate($this->dateFrom);
        $this->dateTo = $this->toStorageDate($this->dateTo);

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()
            ->select([
                '{{INVOICES}}.*, {{SOURCE}}.name',
                'round(sum({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty),2) as total_sum_without_tax',
                'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*{{INVOICES}}.tax_rate)/100,2) ELSE 0 END)  as total_sum_tax',
                'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*(100+{{INVOICES}}.tax_rate))/100,2) ELSE round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) END)  as total_sum_with_tax',
            ])
            ->joinWith('source')
            ->joinWith('invoiceLineItem')
            ->groupBy('{{INVOICES}}.id')
            ->orderBy('{{INVOICES}}.inv_date desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if (!is_null($this->dateFrom)) {
            $query->andFilterWhere([
                '>=',
                'inv_date',
                $this->dateFrom
            ]);
        }
        if (!is_null($this->dateTo)) {
            $query->andFilterWhere([
                '<=',
                'inv_date',
                $this->dateTo
            ]);
        }
        if (!is_null($this->status)) {
            $query->andFilterWhere([
                '=',
                'status',
                $this->status
            ]);
        }
        if (!is_null($this->source_id)) {
            $query->andFilterWhere([
                '=',
                'source_id',
                $this->source_id
            ]);
        }
        $this->dateFrom = $this->toAppDate($this->dateFrom);
        $this->dateTo = $this->toAppDate($this->dateTo);

        return $dataProvider;
    }

    public static function getStatus()
    {
        $result = [

            1 => 'Unpaid',
            2 => 'Paid',
            -1 => 'In Progress',
            3 => 'Voided'
        ];

        return $result;
    }
}
