<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP_ACTION}}".
 *
 * @property string $id
 * @property string $opp
 * @property string $create_date
 * @property string $priority
 */
class OpportunityAction extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP_ACTION}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['id'],
                'required'
            ],
            [
                ['create_date'],
                'safe'
            ],
            [
                [
                    'id',
                    'opp'
                ],
                'string',
                'max' => 32
            ],
            [
                ['priority'],
                'string',
                'max' => 1
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opp' => 'Opp',
            'create_date' => 'Create Date',
            'priority' => 'Priority',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpportunity()
    {
        return $this->hasOne(Opportunity::className(), ['id' => 'opp']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpportunityActionItem()
    {
        return $this->hasMany(OpportunityActionItem::className(), ['opp_action' => 'opp']);
    }
}
