<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%SUBSCRIPTIONS}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $recurrence
 * @property double $discount
 * @property integer $active
 */
class Subscription extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%SUBSCRIPTIONS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'recurrence',
                    'active'
                ],
                'integer'
            ],
            [
                ['discount'],
                'number'
            ],
            [
                ['name'],
                'string',
                'max' => 25
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'recurrence' => 'Recurrence',
            'discount' => 'Discount',
            'active' => 'Active',
        ];
    }

    public static function getAll($all = false)
    {
        $query = self::find();
        if (!$all) {
            $query->where(['active' => 1]);
        }

        return $query->all();
    }
}
