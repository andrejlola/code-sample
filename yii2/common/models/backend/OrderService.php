<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%ORDERS_SERVICES}}".
 *
 * @property integer $order_id
 * @property integer $service_id
 * @property double $price
 * @property double $base_discount
 * @property double $coupon_discount
 */
class OrderService extends \common\components\ActiveRecord
{
    private $serviceObject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ORDERS_SERVICES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'service_id'
                ],
                'integer'
            ],
            [
                [
                    'price',
                    'base_discount',
                    'coupon_discount'
                ],
                'number'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'service_id' => 'Service ID',
            'price' => 'Price',
            'base_discount' => 'Base Discount',
            'coupon_discount' => 'Coupon Discount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return Service
     */
    public function getServiceObject()
    {
        if (is_null($this->serviceObject)) {
            $this->serviceObject = $this->getService()
                ->one();
        }

        return $this->serviceObject;
    }

    public function getPriceWithDiscount()
    {
        $price = $this->price;
        if ($this->base_discount > 0) {
            $price = $price - $price * $this->base_discount;
        }
        if ($this->coupon_discount > 0) {
            $price = $price - $price * $this->coupon_discount;
        }

        return $price;
    }
}
