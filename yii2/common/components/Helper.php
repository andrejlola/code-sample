<?php

namespace common\components;

use Yii;
use yii\i18n\Formatter;
use common\models\backend\Category;

class Helper
{

    public static function getAppDb()
    {
        if (preg_match('/dbname=([A-Za-z]+)/', Category::getDb()->dsn, $matches)) {
            return $matches[1];
        } else {
            return '';
        }
    }


    public function getDateFormat()
    {
        return 'm/d/Y';
    }

    public function getDateTimeFormat()
    {
        return 'm/d/Y H:i:s';
    }

    public function getStorageDateFormat()
    {
        return 'Y-m-d';
    }

    public function getStorageDateTimeFormat()
    {
        return 'Y-m-d H:i:s';
    }


    public static function toStorageDate($value)
    {
        if ($value = strtotime($value)) {
            return date(self::getStorageDateFormat(), $value);
        }

        return null;
    }

    public static function toAppDate($value)
    {
        if ($value && $value != '0000-00-00') {
            if ($value = strtotime($value)) {
                return date(self::getDateFormat(), $value);
            }
        }

        return '';
    }

    public static function formatNumber($value,$sign='$')
    {
        if ($value==0) {
            return '--';
        }
        else {
            return $sign . ' ' . Yii::$app->formatter->asDecimal($value, 2);
        }
    }

    public static function formatPercent($value,$sign='%')
    {
        if (is_numeric($value)) {
            return Yii::$app->formatter->asPercent($value,2);
        }

        return '--';
    }

    public static function getIntegerValue($value)
    {
        return isset($value)?$value:0;
    }

    public static function getCurrentYearDate()
    {
        return date("Y").'-1-1';
    }

    public static function getCurrentYear()
    {
        return date("Y");
    }

    public static function getNextYearDate()
    {
        return (date("Y")+1).'-1-1';
    }

}
