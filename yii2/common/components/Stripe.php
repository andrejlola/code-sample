<?php

namespace common\components;

use Stripe\Customer;

class Stripe extends \yii\base\Component
{
    protected static function getApiKey()
    {
        return \Yii::$app->params['stripe']['secretKey'];
    }

    protected static function ApplyApiKey()
    {
        \Stripe\Stripe::setApiKey(self::getApiKey());
    }

    public static function createCustomer($stripeToken, $subscriptionId, $billingEmail, $billingFirstname, $billingLastname)
    {
        self::ApplyApiKey();
        $customer = Customer::create([
            'source' => $stripeToken,
            'plan' => $subscriptionId,
            'email' => $billingEmail,
            'description' => $billingFirstname . ' ' . $billingLastname
        ]);

        return $customer;
    }

    public static function getCustomer($id)
    {
        self::ApplyApiKey();

        return Customer::retrieve($id);
    }

    public static function DeleteCard($customerId)
    {
        self::ApplyApiKey();
        $result = false;
        $customer = self::getCustomer($customerId);
        foreach ($customer->sources->data as $card) {
            $result = $card->delete();
        }

        return $result;
    }

    public static function AddCard($customerId, $cardId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return $customer->sources->create(['card' => $cardId]);
    }

    public static function HasSubscription($customerId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return count($customer->subscriptions->all()) > 0;
    }

    public static function CancelSubscription($customerId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return $customer->cancelSubscription();
    }
}
