<?php
/**
 * Created by IntelliJ IDEA.
 * User: vovan
 * Date: 22.12.16
 * Time: 16:47
 */

namespace common\components;

use common\models\backend\AFFILIATECODESSBR;
use common\models\backend\AFFILIATESSBR;
use common\models\backend\LICENSESBR;
use common\models\backend\USER;
use common\models\backend\Address;
use common\models\frontend\Preference;
use yii\web\NotFoundHttpException;

use Yii;


class TaxSpecialist extends \common\components\ActiveRecord
{

    public static function getByCode($code)
    {
        /*
        $obj = new AFFILIATECODESSBR();
        $result = $obj::find()->where(['code' => $code])->one();
        if (count($result)>0){
           return $result["affiliate_id"];
        }
        */
        $obj = new AFFILIATESSBR();
        $result = $obj::find()->where(['reg_code' => $code])
            ->andWhere(['not', ['user_id' => null]])
            ->one();
        if (count($result) > 0) {
            return $result["user_id"];
        }

        /*
        $obj = new LICENSESBR();
        $result = $obj::find()->where(['id' => $code])
            ->andWhere(['not', ['user_id' => null]])->one();
        if (count($result)>0){
            return $result["aff_id"];
        }
        */
        return null;
    }

    public static function getSpecialistID()
    {
        $user_id = null;
        $obj = new Preference();
        $result = $obj::find()->where([
            'pref_userid' => Yii::$app->user->id,
            'pref_key' => 'auth_accountant'
        ])->one();

        if ($result->pref_value != '') {
            $user_id = $result["pref_value"];
        } else {
            $result = $obj::find()->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'not_auth_accountant'
            ])->one();
            if ($result) {
                if ($result->pref_value != '') {
                    $user_id = $result["pref_value"];
                }
            }
        }

        return $user_id;
    }

    public static function  CheckDataStructure($user_id, $show_message)
    {

        $obj = new USER();
        $result = $obj::find()->where(['id' => $user_id]);
        $result = $result->one();
        if ($result) {
            $mailing_addr_id = $result->mailing_addr_id;
            $connection = Yii::$app->getDb();
            try {
                $command = $connection->createCommand("SELECT * FROM " . $user_id . ".ADDRESS WHERE id='" . $mailing_addr_id . "' ");
                $result = $command->queryOne();
            } catch (\Exception $e) {
                if ($show_message) {
                    Yii::$app->getSession()->setFlash('error', "Affiliates user have not a database!");
                }

                return null;
            }

            return $result;
        } else {
            if ($show_message) {
                Yii::$app->getSession()->setFlash('error', "Affiliate have not a user!");
            };
        }

        return null;
    }

    public static function getSpecialistInfo($show_message = false)
    {
        $user_id = self::getSpecialistID();
        if (!$user_id) {
            return null;
        }

        return self::CheckDataStructure($user_id, $show_message);
    }
}
