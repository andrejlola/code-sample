<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Learn more';
?>
<h1>Learn more</h1>
<div class="learn-more">
    <div class="tab-button">
        <?php echo Html::a( "System Requirements", ['site/requirements'],  ["title"=>"System Requirements"]); ?>
        <?php echo Html::a( "Data Security", ['site/security'],  ["title"=>"Data Security"]); ?>
    </div>
    <p class="style3">Take just five minutes to read through this page. Trust us - you will learn all about KeepMore.net  and how to Keep More!</p>

    <div class="aset">
        <p>KeepMore.net  is the simplest expense tracking solution for your business. Ask yourself, how do you currently keep track of and manage all of the receipts, income and expense of your business? Maybe a set of file folders - maybe an excel spreadsheet - maybe QuickBooksTM - or maybe a real shoebox!</p>
        <br>
        <p>That's why KeepMore.net  was created. Of all the different expense tracking options, KeepMore.net  gives you five critical benefits: </p>
        <div class="left"><img src="/images/screen1.png" alt=""></div>
        <div class="right">
            <ol>
                <li>a <strong>simple</strong> interface to <strong>rapidly</strong> enter all your business transactions and activity </li>
                <li><strong>immediate</strong> feedback on how your business is doing with no accounting, organization or reporting on your side </li>
                <li>secured <strong>data backup</strong> - no more hard drive crash or email virus worries </li>
                <li>access to your account from <strong>anywhere</strong>, <strong>anytime</strong> and no software to download </li>
                <li>with a <strong>single click</strong> of the mouse, your tax return reports are ready </li>
            </ol>
            <br>
            <p>KeepMore.net  is very different from other accounting software. As you can see, it is not gray and boring. It is activity based and been designed for you the business owner, not the fulltime accounting professional. There are five main activities in KeepMore.net : </p>
            <br>
            <ol>
                <li><u>Enter Transactions:</u> enter all Funds In and Funds Out of your business</li>
                <li><u>Track Auto:</u> rapidly track automobile mileage for all autos used in your business</li>
                <li><u>Track Invoices:</u> a customizable, rapid invoice generator and organizer</li>
                <li><u>Generate Reports:</u> get tax reports, income reports and many others</li>
                <li><u>Balance Accounts:</u> reconcile any bank or credit card account</li>
            </ol>
        </div>

    </div>

    <div class="aset">
        <p>To illustrate how simple KeepMore.net  is to use, let's quickly and simply enter some business activity.</p>
        <p>First let's enter a transaction. This is the screen where you rapidly enter all funds going in to or out of your business. Here's what you should notice; with a receipt or commissions check in hand, you: </p>
        <div class="left1">
            <ol>
                <li>quickly enter the transaction, </li>
                <li>need no accounting knowledge to enter it and </li>
                <li>have an instant financial dashboard about your company's performance! </li>
            </ol>
            <br>
            <p>When entering a transaction, the first field is <u>Transaction Type.</u> You select either Funds In or Funds Out. There is no "credit" or "debit" or "thirteenth period" here. This is simple, single entry bookkeeping - no double entry required! </p>
            <br>
            <p>Next you select which <u>Account</u> (any banking, credit card or cash account) the funds went in to or out of.</p>
            <br>
            <p>Next you select the <u>Method</u> (check, credit card, cash, electronic, etc.) - how did the funds get there? Then you input the <u>Date.</u> </p>
            <br>
            <p>So far so good - no accounting knowledge needed, very intuitive, and very rapid entry.</p>
            <br>
            <p>Next you select the <u>To/From</u> (vendors or customer) - where did the funds go TO or come FROM.</p>
            <br>
            <p>By selecting a To/From, the next two fields automatically get pre-populated for you.</p>
        </div>
        <div class="right1"><img src="/images/screen2.png" alt=""></div>
        <div class="bottom">
            <p><u>Category</u> is a list of IRS tax designations. By selecting a category, you are, in the end, describing to the IRS under which bucket each transaction belongs. If you are not sure for any given expense (telephone services for example) or income item, simply use the Guide Me tool. Guide Me is an online, search engine that returns the appropriate IRS Category for any business expense - saving you valuable time! </p>
            <br>
            <p>Next is <u>Custom Tags;</u> a list you can create, own and modify. Custom Tags allow you to tag the transaction in a meaningful way for your business.</p>
            <br>
            <p>Lastly you enter an <u>Amount</u> and a <u>Memo</u> and your data entry is complete. No accounting knowledge needed! And at this point, you know 95% of all you need to know about recording funds going in and out of your business. </p>
            <div class="left"><img src="/images/screen3.png" alt=""></div>
            <div class="right">
                <p>You click Save & New and instantly you get critical feedback and information about your business: </p>
                <br>
                <ol>
                    <li>Running <u>account balances</u> are displayed next to each account name </li>
                    <li>The <u>Last Five Entries</u> shows you the last entries made by account so you never lose your place and know just where you left off </li>
                    <li>Most importantly, the <u>YTD Bottom Line</u> gives you a true snap-shot of just how your business is performing year-to-date. Income and Expense tell you all the income and expense transactions over all accounts and categories and even shows you your Net Income. The Funds In / Funds Out tells you the cash flow of your business - without any accounting or reporting, you know exactly the money situation of your business! </li>
                </ol>
                <br>
                <p><strong>And that's it for Enter Transactions, it's that easy.</strong> Now let's take a look at Track Auto.</p>
            </div>
        </div>
    </div>

    <div class="aset">
        <p>Most businesses miss out on the <strong>incredible deductions</strong> that a business automobile offers. Track Auto is a <strong>quick and easy</strong> way to track the mileage of all the automobiles in your household. Even if you typically use one car primarily for your business, be sure to add the others into KeepMore.net  because undoubtedly you will take that other car to the bank, airport, or somewhere for business - and those are tax deductions <strong>you don't want to miss!</strong> </p>
        <div class="left"><img src="/images/screen4.png" alt=""></div>
        <div class="right">
            <p>You can enter mileage by either Starting and Ending Odometer readings (which the IRS prefers) or by Business Trip Mileage. Enter the date and reason for the business trip and you are all done! By entering your mileage here, you get instant feedback to your total business mileage and even your estimated tax deduction for the end of the year. We'll take care of keeping up with the IRS and all of their tax code changes so you can focus on building your business.</p>
            <br>
            <p>Believe it or not, it's that simple, and our philosophy has held true. In a matter of 2 to 3, maybe 5, minutes, you can log into your account, enter your business transactions and auto mileage, and get back key information about your business. As the business owner, now you know your business.  </p>
        </div>
    </div>

    <div class="aset">
        <p>Now let's says it is the end of the month or quarter or even the end of the year and you want to get more detailed information about your business. Let's take a look at Generate Reports.</p>
        <br>
        <p><strong>With one click,</strong> you can generate a formatted and fully itemized <u>Income and Expense Report</u> based on your expense categories. <strong>Now you really know your business</strong> and have an instant window into where you are spending money and how profitable you are - <strong>very powerful!</strong> </p>
        <div class="left1">
            <p>At the end of the year, you will select the <u>Schedule C Worksheet Report</u> and again, with one click, your entire business is organized by the IRS required categories. You can either hand this report to your tax advisor (and they will love you!) or plug these numbers into your self-file form or software and your tax return is done!</p>
            <br>
            <p>If your business is an S-Corp or LLC, don't worry, you too are covered with complete <u>Income and Expense</u> and <u>Balance Sheet Reports.</u> </p>
            <br>

            <p><strong>Successful business owners always make sure of two things:</strong></p>
            <br>
            <ol>
                <li><strong>They stay 100% focused on building their business</strong></li>
                <li><strong>They know their business.</strong> </li>
            </ol>
            <br>
            <p><strong>KeepMore.net  today and <u>Keep More!</u></strong></p>
            <br>
            <h2><a href="<?= $urlsecure ?>/reg/">Signing up is easy - click here.</a></h2>
        </div>
        <div class="right1"><img src="/images/screen5.png" alt="" ></div>
    </div>


</div>
