<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'No panic necessary.';
?>
<h1> No panic necessary.</h1>
<div class="container-left1 live-demo1" id="contentPromoLeft">
    <div class="col-det1"><img src="/images/acct_mind.jpg" class="imageLeftWrap" alt="image" /></div>
    <div class="right-sec" id="pageContent">
        <p>
        With KeepMore<sup>TM</sup>, you don't need to know accounting. The interface is straightforward and thorough. You simply enter your daily information, and the data is sorted out for you.
        </p>
        <p>
        ...And because KeepMore is internet-based, it's our duty to make sure you have the latest tax laws factored into your bookkeeping. Automatically. Nothing new to download. Just log in to your account, and everything is ready for you.
        </p><br>
        <p>
            <?php echo Html::a( "Want to learn more?", ['site/learnmore'],  ["title"=>"Want to learn more?", "class"=>"link_cls"]); ?>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
