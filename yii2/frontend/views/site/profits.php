<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'You will know, instantly.';
?>
<h1>You will know, instantly.</h1>
<div class="container-left1 live-demo1" id="contentPromoLeft">
    <div class="col-det1"><img src="/images/profit_dollars.jpg" class="imageLeftWrap" alt="image" /></div>
    <div class="right-sec" id="pageContent">
        <p>
            Money money money. How much you made, how much you spent. All of the answers you need are right here.
        </p>
        <p>
            You enter your transactions: what money you make, what money you spend. Then with one click, you have a report in your hands that will tell you how profitable your business is. It really is that simple.
        </p>
        <p>
            Since KeepMore<sup>TM</sup> is password-protected, secure and 100% backed up daily, the only person knowing your business is YOU.        </p>

        <br>
        <p>
            <?php echo Html::a( "Want to learn more?", ['site/learnmore'],  ["title"=>"Want to learn more?", "class"=>"link_cls"]); ?>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
