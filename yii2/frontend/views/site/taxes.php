<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'More spare time.';
?>
<h1>More spare time.</h1>
<div class="container-left1 live-demo1" id="contentPromoLeft">
    <div class="col-det1"><img src="/images/taxes_stack.jpg" class="imageLeftWrap" alt="image" /></div>
    <div class="right-sec" id="pageContent">
        <p>
            Now your taxes will be ready in a jiffy. Gone are the days of pulling together scraps of paper and receipts.
            All of your transactions will be in one place. At the end of the year, select "Schedule C Worksheet Report" and in the blink of an eye,
            your business is organized by the IRS required categories.
        </p>
        <p>
            Print it out, and either hand it to your accountant (who will love you for this, by the way), or simply plug in the numbers into your self-filing
            form or software, and you are set.
        </p>
        <p>
            If your business is an S-Corp or LLC, don't worry, you too are covered with complete <u>Income and Expense</u> and <u>Balance Sheet Reports</u>.
        </p>
        <p>
            Since KeepMore<sup>TM</sup> is password-protected, secure and 100% backed up daily, you don't ever have to worry about your tax information being lost or stolen.
        </p>

        <br>
        <p>
            <?php echo Html::a( "Want to learn more?", ['site/learnmore'],  ["title"=>"Want to learn more?", "class"=>"link_cls"]); ?>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
