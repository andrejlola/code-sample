<?php
use yii\helpers\Html;
?>
<div class="container-fluid " style ="position: relative;left:0px;">
    <div >
        <img src="/images/banner.jpg" width="100%" >
    </div>
    <div  style ="position: absolute;top:0px;width:100%">
        <div class="container" style="position: relative;">
            <div id="joinus-bx">
                <div class="joinus-bx1" id="contentTaxTips">
                    <h2>Get My Tips</h2>
                    <form method="post" action="/" name="postForm">
                        <input type="hidden" name="action" value="email"/>
                        <p><input type="text" name="name" value="Your name" class="frmfield2" ></p>
                        <p><input type="text" name="email" value="Your e-mail" class="frmfield2"></p>
                        <p><a style="cursor:pointer;"  class="get-btn"></a></p>
                    </form>
                    <div class="tips-banner"><img src="/images/tips-banner.png" alt="Tips"></div>
                    <p class="arrow"><img src="/images/arrow.png" alt="Arrow"></p>
                </div>
            </div>
        </div>
    </div>

</div> <!-- /container -->
<div class="container-fluid banner-bott" id="follow">
    <div class="container" >
        <div class="row">
            <div class="col-lg-8 col-md-4 col-sm-7 col-xs-12">
                <div class="banner-bott-left">
                    <p>You're about to try out KeepMore, using our live demo.</p>
                    <p class="style">
                        Click Here to Launch the KeepMore
                        <a title="Demo" href="?page=demo">Live Demo</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 " >
                <div class="social ">
                    <p>
                        <span>Follow Us</span>
                        <span><a href="https://www.facebook.com/keepmorenet" target="_blank" title="Facebook" ><img src="/images/facebook.jpg" alt="Facebook"></a></span>
                        <span><a href="https://www.youtube.com/channel/UClDoJM6GPsNsOpFkpdxv6Pw" target="_blank" title="Youtube"><img src="/images/you-tube.jpg" alt="Youtube"></a></span>
                        <span><a href="https://twitter.com/keepmorenet/lists/keepmore-net" target="_blank" title="Twitter" ><img src="/images/twitter.jpg" alt=""></a></span>
                        <span><a href="https://www.instagram.com/keepmorenet" target="_blank" title="Instagram"><img src="/images/in.jpg" alt=""></a></span>
                        <span><a href="https://pinterest.com/guykeepmore/keepmorenet/" target="_blank" title="Pinterest"><img src="/images/pinterest.jpg" alt=""></a></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container" id="index_main_content" >
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 main-content-left" >
            <h2>Smile</h2>
            <p>Your life is about to get a whole lot easier. KeepMore&trade; is your all-in-one online business management solution that simplifies your business! </p>
            <br />
            <h2>Happy wallet</h2>
            <p>It starts with accounting made simple so you can quickly keep track of your income, expenses, mileage and receipts</p>
            <p>...which leads to you keeping more of your hard-earned money.</p>
            <br>
            <h2>Happy customers</h2>
            <p>Next, KeepMore helps you turn more prospects into customers with our contact manager called Opportunity Tracker. You'll always remember to follow-up with automatic email reminders... </p>
            <p>...so you earn more money!</p>
            <br>
            <h2>Happy you</h2>
            <p>Automate and simplify your life by using KeepMore. Gone are the days of stuffing receipts into boxes, spending hours and hours doing taxes, losing business cards, and forgetting to call a prospect back - KeepMore is all you need. Easily keep track of your money, expenses, and prospects for less than $20/month.</p>

            <div class="bottom-bx">
                <h2>Take us for a test drive using our Live Demo. See how easy it is to smile!</h2>
                <ul>
                    <li><?php echo Html::a( "I need organization", ['site/organization'],  ["title"=>"I need organization"]); ?></li>
                    <li><?php echo Html::a( "I don't know accounting", ['site/accounting'],  ["title"=>"I don't know accounting"]); ?></li>
                    <li><?php echo Html::a( "I need to see my profits -vs- my expenses", ['site/profits'],  ["title"=>"I need to see my profits -vs- my expenses"]); ?></li>
                    <li><?php echo Html::a( "I don't know what is deductible", ['site/deductible'],  ["title"=>"I don't know what is deductible"]); ?></li>
                    <li><?php echo Html::a( "Tax time is a chore", ['site/taxes'],  ["title"=>"Tax time is a chore"]); ?></li>
                </ul>
            </div>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $this->render( 'right_banners'); ?>
        </div>
    </div>
