<?php

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
