<?php

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */

$this->title = 'Update';

$this->params['breadcrumbs'][] = 'Update';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
