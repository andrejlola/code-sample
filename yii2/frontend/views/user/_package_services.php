<?php

/* @var $this yii\web\View */
/* @var $model common\models\backend\Order */
/* @var $orderService common\models\backend\OrderService */
?>
<?php foreach ($model->getSubscriptionPriceCases() as $subscriptionId => $orderServices) { ?>
    <table class="table service-price" style="<?= ($subscriptionId == $model->subscription_id)
        ? ''
        : 'display:none;' ?>" data-subscription_id="<?= $subscriptionId ?>">
        <?php foreach ($orderServices as $orderService) { ?>
            <tr>
                <td><img src="/images/tick.png"><?= $orderService->getServiceObject()->name ?></td>
                <td><?= ($orderService->price == $orderService->getPriceWithDiscount())
                        ? ''
                        : '<span class="text-danger"><del>' . Yii::$app->formatter->asCurrency($orderService->price)
                        . '</del></span>'; ?> <?= Yii::$app->formatter->asCurrency($orderService->getPriceWithDiscount()) ?></td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>
