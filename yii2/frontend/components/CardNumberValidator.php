<?php
namespace frontend\components;

use yii\validators\Validator;

class CardNumberValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'Invalid card number.';
    }

    public function validateAttribute($model, $attribute)
    {
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS

/*if ($.inArray(value, ['123']) === -1) {
    messages.push($message);
}*/

JS;
    }
}

