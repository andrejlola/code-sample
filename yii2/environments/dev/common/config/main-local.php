<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=keepmore',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'app_db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname={dbname}',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'urlManagerFront' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://keepmore.testsoft.org',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'urlManagerBack' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://backend-keepmore.testsoft.org',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'urlManagerApp' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://app-keepmore.testsoft.org',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'helper' => [
            'class' => 'common\components\Helper',
        ]
    ],
];
