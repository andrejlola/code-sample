<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Report  form
 */
class ReportForm extends Model
{
    public $report;
    public $year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'year'
                ],
                'required'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report' => 'Select Report',
        ];
    }
}
