<?php
namespace app\modules\communicate\components;

use Yii;
use common\models\backend\Message;

/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/communicate' => 'Messages',
        //'/auto/expense' => 'View/Edit Actual Expense',
        //'/auto/manage-auto' => 'Manage Autos',
        '/auto/first-time' => 'First Time In Help',
    ];

}
