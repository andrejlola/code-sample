<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\backend\Message;
use kartik\select2\Select2;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var common\models\frontend\AutoMileage */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'message-form',
        'class' => 'form-horizontal',
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
    ],
]); ?>

<?= $form->field($model, 'to_id')
    ->widget(Select2::classname(), [
        'data' => Message::getToUser(),
    ])
    ->label('Message To') ?>

<?= $form->field($model, 'type')
    ->widget(Select2::classname(), [
        'data' => Message::getTypes(),
        'options' => ['placeholder' => 'Select A Message Type'],
    ]) ?>
<?= $form->field($model, 'subject')
    ->textInput();?>

<?= $form->field($model, 'message')
    ->textarea();?>


<?= $form->field($model, 'from_id')
    ->hiddenInput(['value'=>Yii::$app->session->get('__id')])->label(false); ?>


<div class="form-group modal-footer">
    <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>' . ($model->isNewRecord
            ? ' Add'
            : ' Update'), [
        'class' => 'btn btn-success'
    ]) ?>
    <button type="button" class="btn btn-success" data-dismiss="modal"><span
            class="glyphicon glyphicon-ban-circle"></span> Cancel
    </button>

</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
