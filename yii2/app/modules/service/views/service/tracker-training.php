<?php
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<p>
    <b>Please carefully read</b> through the following information and training for Opportunity Tracker.
</p>
<p>
    If you do not wish complete the training right now,
    <a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">click here</a> to return to Premium Services.
</p>
<p>
    You have 2 training options. Watch our FREE web-based training session here:
    <b><a href="http://gosolo1.centra.com/main/User/GuestPlayback.jhtml?s_guid=0000004a2dd400000108fcd01f43ab68" target="_new">Launch OT Training</a></b> (Recommended).
</p>
<p>
    Or read the detailed instructions below.
</p>
<hr>
<h3>Opportunity Tracker</h3>
<p><img alt='Opportunity Tracker' src='/images/opp.gif' border=0 align=left hspace=10>Opportunity Tracker ("OT") provides a very simple and effective method for you to follow-up with your prospects, customers, clients and other contacts.  Not only does <? echo $COMPANY ?> help you save more, it can help you make more?..and KEEP MORE of the More!  Now when you entertain a new prospect, contact a current customer or client or just meet with a business associate, you can use Opportunity Tracker to maximize your business efforts.  Using Opportunity Tracker you will be reminded daily of the next email, telephone call or personal contact you need to complete in order to turn those 'Prospects' into Customers, Members, Associates, etc., as quickly as possible.
<p>Access your Opportunity Tracker through the Premium Services activity on your navigation bar.  Once in Premium Services, select 'Opportunity Tracker' under Related Activities.
<p>To populate OT, you can use Enter Transactions, Track Auto or directly enter the Opportunities' information as follow:
<ul>
    <li><b>Track Auto</b> - As you drive around to meet with prospects, customers, clients and other contacts, you will want to record that mileage in Track Auto. Once you have entered the mileage, you can quickly add their contact information or select an existing name in OT by clicking on the OT icon. Now, at the end of the day, month or year, you can see how many miles you are driving per customer or opportunity.
    <li><b>Enter Transactions</b> - Assume you have a lunch receipt as a result of meeting with a prospect, customer, clients or other contact.  Once you have entered all the information related to this expense in Enter Transactions, you can quickly add their contact information or select an existing name in OT by clicking on the OT icon. Now, at the end of the day, month or year, you can see how much you are spending on each customer or opportunity.
    <li><b>Opportunity Tracker Main Screen</b> - As you go through your day, if you think of a new prospect, customer, client or other contact you want to contact in the future, you can quickly add their name to OT by selecting Add Opportunity in the Opportunity Tracker main screen.
    <li><b>Telephone Updates</b> - This is also the area where you will record various phone conversations and follow-up requirements.  In this case, you may not have a related business expense or mileage to report, but you want to capture the details of that last conversation.  Perhaps your prospect told you they wanted to become a customer, and that they sent a check.  Record it!
</ul>
<p>As your list expands, you will be able to quickly select the Opportunity by last name by selecting the appropriate letter in the Alphabetical listing.
<p align=center><img src="/images/ot_main_500.jpg">
<p>Once Opportunity Tracker is open you will have the following additional functions listed to the right under Related Activities:
<ul>
    <li>
        <b>Run Reports</b> - From the Run Reports activity, you open the Opportunity Tracker Call Report.  You can generate and print reports to take with you on the road.
        You have the option of printing the report by the defaulted (current) date, or a date of your preference.  Through the 'Edit Preferences' function explained below,
        you can select to receive an email of your Opportunity Tracker Call Report (handy reminder) on a daily or weekly basis and for a pre-defined number of days.
    </li>
    <li>
        <b>Edit Preferences</b> - Customize your Opportunity Tracker to fit your business preferences and needs.  You can select the Font type and size, select to have Skype turned
        On or Off (if you are Skype user) and the settings for your Opportunity Tracker Call Report.  You can even identify your manager, sponsor, or upline you might want to
        routinely copy about your sales activity.
        <?
        if ($flow!="PPL"){
        ?>
    </li>
    <li>
        <b>Manage Documents</b> - Opportunity Tracker allows you to send emails to your prospects, customers and other contacts listed in OT.  Using 'Manage Documents' you can edit
        standard email templates to fit your business needs.  These stored emails are sent automatically as a follow-up to prospects and customers and can be edited or change
        on as-needed basis.
        <?
        }
        ?>
    </li>
    <li>
        <b>OT Training</b> - Click on OT Training at any time you need a refresher course on how to use OT.
    </li>
</ul>
<p>
    <i>After you finish your training, please take a few minutes now to visit the activities above.</i>
</p>
<p>
    <b>Contact Breakdown</b>
    <br>
    Use the 'Contact Breakdown' meter for a quick snapshot of your contact efforts to date.  Your prospects are always listed in Green to remind you that this is the area
    where you ultimately will grow your business.  Set a goal to always keep this number high and to turn those 'Prospects' into Customers, Members, Associates, etc.,
    as quickly as possible.  This meter gives you valuable information about the successes of you prospecting efforts, giving you a 'Total Contacts'
    listed, and the current status of each.
</p>
<p>
    <b>2006 Success Meter</b>
    <br>
    Use the '2006 Success Meter' to quickly determine which sales materials give you the 'greatest successes'.  This meter will help you stay focused on the sales activities
    that matter the most.
</p>
<p>
    <b>Your list of Opportunities</b>
    <br>
    The list of Opportunities will give you, at a quick glance, valuable information on each Opportunity.  This includes the date Last Contacted, date of next ToDo, Status of Opportunity,
    and Classification (priority).  After you enter each new Opportunity you will be able to edit, track history, or delete the Opportunity by clicking on the applicable icon next to the
    name.
</p>
<ul>
    <li>
        <b>Edit</b> - Click on the 'Edit Pencil' <img src="/images/edit.png" border=0> to see your ToDo list or record new contact information for that Opportunity.
        From here, you can also review or delete each ToDo listed. To edit a ToDo, simply delete the old ToDo and create a new one with the updated information.
    </li>
    <li>
        <b>History</b> - A click on the 'History' <img src="/images/history.gif" border=0> button will open a list of all the past events created for the selected Opportunity.
        From here, you can review or delete each History item listed. To change a History item, simply delete the old item and generate a new one with the updated information.
    </li>
    <li>
        <b>Delete</b> - You can quickly delete all information related to any Opportunity by clicking on the <img src="/images/delete.png" border=0> button.  Be careful!
        This will delete all information entered to date.  You will be asked to confirm you selection to delete all information.
    </li>
</ul>

<b>Look for more up coming training for Opportunity Tracker. In the meantime, we wish you much success!</b>

<?php
    $form = ActiveForm::begin([
        'action' => Url::to(['tracker-enable'])
    ]);

    echo $form->field($model, 'flow')
         ->hiddenInput(['value' => $flow])
        ->label(false);

    echo Html::submitButton('I am ready to go!', [
        'class' => 'btn btn-success'
    ]);
    $form::end();
?>
