<?php

namespace app\modules\service\controllers;

use Yii;
use app\modules\service\components\Controller;
use common\components\OpportunityTracker;
use yii\helpers\Url;

/**
 * OpportunityController
 */
class OpportunityController extends Controller
{
    public function actionIndex()
    {
        if (!OpportunityTracker::isEnabled()) {
            return $this->redirect(Url::to(['/service/opportunity-marketing']));
        }

        return $this->render('index');
    }
}
