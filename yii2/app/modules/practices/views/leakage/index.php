<p>
    Take a minute to review whether or not you are fully realizing all of the tax benefits available to you.
    One area to examine closely is your auto expense.  It might not seem worth it to take the time to document every little business trip,
    especially with the many different ways you can account for your auto expense, but that is the classic problem.<br>
    It is those small trips and record keeping inaccuracies that can add up to hundreds if not thousands of dollars of lost expenses at the end of the year.<br><br>
    Our recommendation:  (1) find the reporting method that fits your business?
    If you are not sure, it is worth talking to a tax advisor and (2) be diligent ? use <?php echo  Yii::$app->params['companyName']; ?> to rapidly enter your business auto expenses
    and mileage and watch the tax savings accumulate.
</p>
<p>
    If you travel just 10,000 business miles in 2005, you can reduce your income approximately $4050.00.  Depending on your effective tax rate,
    you can save somewhere between $400.00 and $1500.00 - that?s real money in your pocket.  This saving alone will more than pay for <?php echo  Yii::$app->params['companyName']; ?>!
</p>
