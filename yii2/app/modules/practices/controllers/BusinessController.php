<?php
namespace app\modules\practices\controllers;

use Yii;
use app\modules\practices\components\Controller;

/**
 * BusinessController
 */
class BusinessController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
