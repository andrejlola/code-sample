<?php
namespace app\modules\report\controllers;


use common\models\frontend\AutoMileage;
use Yii;
use app\modules\report\components\Controller;
use app\models\ReportForm;
use app\models\ReportFilterForm;
use app\modules\report\components\Report;
use common\models\frontend\Invoice;
use common\models\frontend\Reconcile;
use yii\web\Response;
use common\components\Helper;
use common\models\frontend\Transaction;
use common\models\frontend\Auto;

class ReportController extends Controller
{
    public function actionIndex()
    {
        $model = new ReportForm();

        $request = Yii::$app->request->post('ReportForm');
        $year = date('Y');
        $year_date = null;
        if ($request['year']!='') {
            $year = $request['year'];
            $year_date = $year.'-01-01';
        }


        $model->year = $year;

        $data = Transaction::getInfoWidgetData($year_date);
        $resultTransaction = [
            'header' => $year . ' YTD Bottom Line',
            'items' => [
                'net-income' => [
                    'items' => [
                        'Income' => Helper::formatNumber($data["net_income"]["income"],''),
                        'Expense' => Helper::formatNumber($data["net_income"]["expense"],''),
                        'Net Income' => Helper::formatNumber($data["net_income"]["net_income"],'')
                    ],
                    'label' => 'Net Income',
                ],
                'funds-in-out' => [
                    'items' => [
                        'Funds In' => Helper::formatNumber($data["funds_in_out"]["funds_in"],''),
                        'Funds Out' => Helper::formatNumber($data["funds_in_out"]["funds_out"],''),
                        'Total' => Helper::formatNumber($data["funds_in_out"]["total"],''),
                    ],
                    'label' =>'Funds In/Out'
                ],
            ],
        ];


        $businessMiles = Auto::getTotalMileage(true,$year_date);
        $actualExpense = Auto::getActualExpense($year_date);
        $estMileageDed = Auto::getMileageDed($year_date);
        $estActualDed = Auto::getEstimatedDeduction($year_date);

        $resultAuto = [
            'header' => $year . ' YTD AUTO METER',
            'items' => [
                'METER1' => [
                    'items' => [
                        'Business Miles' => Helper::formatNumber($businessMiles,''),
                        'Actual Expense' => Helper::formatNumber($actualExpense,''),
                        'Est Mileage Ded' => Helper::formatNumber($estMileageDed),
                        'Est Actual Ded' => Helper::formatNumber($estActualDed,''),
                    ],
                    'actions' => [
                    ]

                ],
            ],
        ];
        $annualData = Auto::needAutoAnnual($year);
        if (count($annualData)>0) {
            $resultAuto["items"]["METER1"]["actions"] = $annualData;
        }


        $lastTransaction = Transaction::getLast($year_date);
        $lastTransaction = is_null($lastTransaction)?'None Entered':$lastTransaction;

        $lastMileage = AutoMileage::getLast($year_date);
        $lastMileage = is_null($lastMileage)?'None Entered':$lastMileage;

        $lastReconcile = Reconcile::getLast($year);
        $lastReconcile = is_null($lastReconcile)?'None Balanced':$lastReconcile;
        $resultStat = [
            'header' => $year . ' Date Watch',
            'items' => [
                'METER2' => [
                    'items' => [
                        'Last Transaction' =>$lastTransaction,
                        'Last Mileage' =>$lastMileage,
                        'Last Reconcile' =>$lastReconcile,
                    ],
                ],
            ],
        ];

        return $this->render('index', [
            'model' => $model,
            'infoTransaction' => $resultTransaction,
            'infoAuto' => $resultAuto,
            'infoStat' => $resultStat
        ]);
    }

    public function actionViewForm()
    {
        $request = Yii::$app->request;
        $reportView = $request->get('report');
        $year = trim($request->get('year'));

        $model = new ReportFilterForm();
        $from = $year.'-01-01';
        $to = $year.'-12-31';

        $model->from = Helper::toAppDate($from);
        $model->to = Helper::toAppDate($to);
        $model->setScenario($reportView);


        if ($reportView == ReportFilterForm::SCENARIO_INVOICE_AGING)
        {
            $data = Invoice::find()
                ->select(
                    'max(inv_date) as max_date, min(inv_date) as min_date'
                )
                ->where('status=1')
                ->one();
        }
        return $this->renderAjax("_" . $reportView, [
            'model' => $model,
            'report' => $reportView,
            'data' => $data
        ]);
    }

    public function actionIncomeAndExpense()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];

        $data = Report::getIncomeAndExpenseData($from, $to);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Income and Expense Report';


        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('income-and-expense', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('income-and-expense', $viewParams);
    }

    public function actionScheduleCWorksheet()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];

        $data = Report::getScheduleCWorksheet($from, $to);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Schedule C Worksheet Report';


        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('schedule-c-worksheet', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('schedule-c-worksheet', $viewParams);
    }

    public function actionMileage()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $auto = null;

        if ($filter['auto'] != "") {
            $auto = implode("','", $filter['auto']);
        }

        $type_view = $filter['type_view'];

        $data = Report::getMileage($from, $to, $auto);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Mileage Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('mileage', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('mileage', $viewParams);
    }

    public function actionAutoActualExpense()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $auto = null;
        if ($filter['auto'] != "") {
            $auto = implode("','", $filter['auto']);
        }
        $type_view = $filter['type_view'];

        $data = Report::getAutoActualExpense($from, $to, $auto);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Actual Auto Expense';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('auto-actual-expense', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('auto-actual-expense', $viewParams);
    }

    public function actionInvoiceActivity()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $source = null;
        if ($filter['source'] != "") {
            $source = implode("','", $filter['source']);
        }
        $type_view = $filter['type_view'];

        $data = Report::getInvoiceActivity($from, $to, $source);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Invoice Activity Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('invoice-activity', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('invoice-activity', $viewParams);
    }

    public function actionInvoiceItemActivity()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $source = null;
        $invoice_item = null;
        if ($filter['source'] != "") {
            $source = implode("','", $filter['source']);
        }
        if ($filter['invoice_item'] != "") {
            $invoice_item = implode("','", $filter['invoice_item']);
        }
        $type_view = $filter['type_view'];

        $data = Report::getInvoiceItemActivity($from, $to, $source,$invoice_item);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Invoice Item Activity Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('invoice-item-activity', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('invoice-item-activity', $viewParams);
    }

    public function actionInvoiceSalesTax()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $sales_tax = null;
        $invoice_item = null;
        if ($filter['invoice_item'] != "") {
            $invoice_item = implode("','", $filter['invoice_item']);
        }
        if ($filter['sales_tax'] != "") {
            $invoice_item = implode("','", $filter['sales_tax']);
        }

        $type_view = $filter['type_view'];

        $data = Report::getInvoiceSalesTax($from, $to, $invoice_item, $sales_tax);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Invoice Sales Tax Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('invoice-sales-tax', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('invoice-sales-tax', $viewParams);
    }

    public function actionInvoiceAging()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $source = null;
        if ($filter['source'] != "") {
            $source = implode("','", $filter['source']);
        }
        $type_view = $filter['type_view'];

        $data = Report::getInvoiceAging($from, $to, $source);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Invoice Aging Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('invoice-aging', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('invoice-aging', $viewParams);
    }


    public function actionBusinessActivity()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $source = null;
        if ($filter['source'] != "") {
            $source = implode("','", $filter['source']);
        }

        $type = null;
        if ($filter['type'] != "") {
            $type = implode("','", $filter['type']);
        }

        $account = null;
        if ($filter['account'] != "") {
            $account = implode("','", $filter['account']);
        }

        $custom_tag = null;
        if ($filter['custom_tag'] != "") {
            $custom_tag = implode("','", $filter['custom_tag']);
        }

        $category = null;
        if ($filter['category'] != "") {
            $category = implode("','", $filter['category']);
        }
        $amount_from = ($filter['amount_from'] != "")?$filter['amount_from']:null;
        $amount_to = ($filter['amount_to'] != "")?$filter['amount_to']:null;

        $type_view = $filter['type_view'];

        $data = Report::getBusinessActivity($from, $to, $type, $account, $source, $amount_from, $amount_to, $custom_tag, $category);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Business Activity Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('business-activity', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('business-activity', $viewParams);
    }

    public function actionReconciledList(){

        $out=[];
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $filter = $request->post('depdrop_all_params');

        $data=Reconcile::find()
            ->select('id,  name')
            ->where(['=', 'account_id', $filter["account_id"] ])
            ->asArray()
            ->all();
        $out['output'] = $data;

        return $out;
    }

    public function actionReconciliation()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $reconciled = $filter['reconciled'];
        $output = $filter['output'];

        $data = Report::getReconciliation($reconciled);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Reconciliation Report';


        $viewParams = [
            'data' => $data,
            'output' => $output
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('reconciliation', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('reconciliation', $viewParams);
    }

    public function actionLoanActivity()
    {
        $request = Yii::$app->request;
        $filter = $request->post('ReportFilterForm');
        $from = $filter['from'];
        $to = $filter['to'];
        $output = $filter['output'];
        $source = null;
        if ($filter['source'] != "") {
            $source = implode("','", $filter['source']);
        }

        $type = null;
        if ($filter['type'] != "") {
            $type = implode("','", $filter['type']);
        }

        $account = null;
        if ($filter['account'] != "") {
            $account = implode("','", $filter['account']);
        }

        $custom_tag = null;
        if ($filter['custom_tag'] != "") {
            $custom_tag = implode("','", $filter['custom_tag']);
        }

        $category = null;
        if ($filter['category'] != "") {
            $category = implode("','", $filter['category']);
        }
        $amount_from = ($filter['amount_from'] != "")?$filter['amount_from']:null;
        $amount_to = ($filter['amount_to'] != "")?$filter['amount_to']:null;

        $type_view = $filter['type_view'];

        $data = Report::getBusinessActivity($from, $to, $type, $account, $source, $amount_from, $amount_to, $custom_tag, $category);
        $this->layout = '@app/views/layouts/report';
        $this->titlePage = 'Loan Activity Report';

        $viewParams = [
            'to' => $to,
            'from' => $from,
            'data' => $data,
            'output' => $output,
            'type_view' => $type_view
        ];

        if ($output == ReportFilterForm::VIEW_PDF) {
            $content = $this->renderPartial('loan-activity', $viewParams);

            $stylesheet = file_get_contents(Yii::getAlias('@webroot') . '/css/pdf/report.css');

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = $stylesheet;
            $pdf->methods = [
                'SetHeader' => [
                    $this->titlePage.', Generated On: ' . date('m/d/Y') . ', Powered By: ' . Yii::$app->params['companyName']
                ],
                'SetFooter' => ['{PAGENO}'],
            ];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');

            return $pdf->render();
        }

        return $this->render('loan-activity', $viewParams);
    }
}
