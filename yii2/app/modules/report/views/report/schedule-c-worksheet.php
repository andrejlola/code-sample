<?php
use common\components\Helper;
use app\models\ReportFilterForm;

?>
    <div style="width:100%; " class="text-center">
        <h1><?=$this->context->titlePage;?></h1>
    </div>
    <div style="width:100%;  " class="text-center">
        <table width="100%" border="0">
            <tr>
                <td align="center"><h4><?= $order->billing_company ?></h4></td>
            </tr>
            <tr>
                <td align="center"><h4><?= $from ?> – <?= $to ?></h4></td>
            </tr>
        </table>
    </div>
    <br>
    <table class="table-bordered-report" width="100%"
        <?php
        $unknown = "";
        if (isset($data["c"]["Unknown category - to be resolved"]) || isset($data["c"]["Imported Items"])) {
            $unknown = "<sup> *</sup>";
        }

        if ($output != ReportFilterForm::VIEW_PDF) {
            echo ' border="1"';
        }
        ?>
        >
        <tr>
            <td colspan="2"><h3>Income</h3></td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Gross receipts or sales
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Gross receipts or sales"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Gross receipts or sales - sales tax collected
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Sales tax collected"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Returns and allowances
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Returns and allowances"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Net receipts and sale
            </td>
            <td class="column-number">
                <?php
                $sub_tot1 = Helper::getIntegerValue($data["c"]["Gross receipts or sales"]) +
                    Helper::getIntegerValue($data["c"]["Sales tax collected"]) +
                    Helper::getIntegerValue($data["c"]["Returns and allowances"]);
                echo Helper::formatNumber($sub_tot1);
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Purchases - goods to be sold
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Purchases - goods to be sold"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Gross profit
            </td>
            <td class="column-number">
                <?php
                $sub_tot2 = $currValue + $sub_tot1;
                echo Helper::formatNumber($sub_tot2);
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="column-text-margin">
                Other income
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Other income"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="bold">
                Gross income <?= $unknown ?>
            </td>
            <td class="column-number bold">
                <?php
                $income = $currValue + $sub_tot2;
                echo Helper::formatNumber($income);
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><h3>Expense<?= $data["c"]["Advertising"] ?></h3></td>
        </tr>
        <?php
        $row = array(
            "Advertising",
            'Car and truck expenses',
            'Commissions and fees',
            'Contract labor',
            'Depletion',
            'Depreciation/Sec 179',
            'Employee benefit programs',
            'Insurance',
            'Interest - mortgage',
            'Interest - other',
            'Legal and professional services',
            'Office expense',
            'Pension and profit-sharing plans',
            'Rent or lease - machinery and equipment',
            'Rent or lease - vehicles',
            'Rent or lease - other business property',
            'Repairs and maintenance',
            'Supplies',
            'Taxes and licenses',
            'Payroll taxes and withholding',
            'Sales tax remitted',
            'Travel, meals, ent. - travel',
            'Travel, meals, ent. - meals & entertainment',
            'Non-deductible amount - travel, meals, entertainment - meals and entertainment',
            'Utilities',
            'Wages'
        );
        while (list($key) = each($row)) {
            ?>

            <tr>
                <?php
                $line_txt = $row[$key];
                $line_amount = Helper::getIntegerValue(-$data["c"]["$row[$key]"]);
                if ($row[$key] == "Car and truck expenses") {
                    $line_txt = "Car and truck expenses<span style='color:red'><i> (based on mileage method)</i></span>";
                    $line_amount = Helper::formatNumber($total_mileage_deduction);
                } elseif ($row[$key] == "Rent or lease - vehicles") {
                    $line_txt = "Rent or lease - vehicles<span style='color:red'><i> (based on mileage method - see Car and truck expenses)</i></span>";
                    $link_line = "<td class='column-text-margin'></td>";
                    $line_amount = "0.00";
                } elseif ($row[$key] == "Payroll taxes and withholding") {
                    $line_txt = "Taxes and licenses - payroll taxes and withholding";
                } elseif ($row[$key] == "Sales tax remitted") {
                    $line_txt = "Taxes and licenses - sales tax remitted";
                } elseif ($row[$key] == "Non-deductible amount - travel, meals, entertainment - meals and entertainment") {
                    $link_line = "<td height='18'></td>";
                    $line_amount = 0.5 * Helper::getIntegerValue($data["c"]["Travel, meals, ent. - meals & entertainment"]);
                }

                ?>
                <td class="column-text-margin"><?= $line_txt ?></td>
                <td class="column-number"><?= Helper::formatNumber($line_amount) ?></td>
            </tr>

        <?php
        }
        ?>
        <tr>
            <td class="column-text-margin">Other expenses<sup> 1</sup></td>
            <td class="column-number">&nbsp;</td>
        </tr>
        <?php
        if (is_array($data["o"])) {
            while (list($key) = each($data["o"])) {
                $line_txt = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $key;
                $line_amount = Helper::formatNumber(-$data["o"]["$key"]);
                ?>
                <tr>
                    <td class="column-text-margin"><?= $line_txt ?></td>
                    <td class="column-number"><?= $line_amount ?></td>
                </tr>
            <?
            }
        }
        $currValue = $data["other_total"] - Helper::getIntegerValue($data["c"]["Other expenses"]);
        if (!isset($data["o"]) || $currValue != 0) {
            ?>
            <tr>
                <td class="column-text-margin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No 'Custom Tag'
                    assigned
                </td>
                <td class="column-number"><?= Helper::formatNumber($currValue) ?></td>
            </tr>

        <?php
        }
        ?>
        <tr>
            <td class="column-text-margin">
                &nbsp;&nbsp;&nbsp;&nbsp;Total Other expenses
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["c"]["Other expenses"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <b>Total expenses <?= $unknown ?></b>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue(-$data["total_expenses"]);
                echo '<b>' . Helper::formatNumber($currValue) . '</b>';
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <b>Net profit or (loss) before business use of your home (if any) <?= $unknown ?></b>
            </td>
            <td class="column-number">
                <?php
                $currValue = $income + Helper::getIntegerValue($data["total_expenses"]);
                echo '<b>' . Helper::formatNumber($currValue) . '</b>';
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table class="table-bordered-report" width="100%"
        <?php
        if ($output != ReportFilterForm::VIEW_PDF) {
            echo ' border="1"';
        }
        ?>
        >
        <tr>
            <td><h3>Other business activity</h3></td>
            <td colspan="2">from <?= $from ?> to <?= $to ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Funds In</td>
            <td>Funds Out</td>
        </tr>
        <?php
        $need_space = false;
        if (isset($data["c"]["Unknown category - to be resolved"])) {
            $need_space = true;
            ?>
            <tr>
                <td class="column-text-margin">Unknown category - to be resolved</td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["ci"]["Unknown category - to be resolved"]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["co"]["Unknown category - to be resolved"]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
            </tr>

        <?php
        }
        if (isset($data["c"]["Imported Items"])) {
            $need_space = true;
            ?>
            <tr>
                <td class="column-text-margin">Imported Items</td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["ci"]["Imported Items"]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["co"]["Imported Items"]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
            </tr>

        <?php
        }
        if ($need_space == true) {
            ?>
            <tr>
                <td colspan="3"></td>
            </tr>
        <?php
        }
        ?>
        <tr>
            <td class="column-text-margin">Owner investment</td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["ci"]["Owner investment"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue($data["co"]["Owner investment"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <?php
        $row = array(
            'Asset',
            'Debt payment',
            'Transfer funds',
            'Owner charitable contributions',
            'Owner draw',
            'Owner income tax payments federal',
            'Owner income tax payments state',
            'Owner health insurance',
            'Prepaid deposits',
            'Purchases - goods for personal use'
        );
        while (list($key) = each($row)) {
            ?>
            <tr>
                <td class="column-text-margin"><?= $row[$key] ?></td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["ci"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["co"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
        <tr>
            <td><h3>Loan activity</h3></td>
            <td colspan="2">from <?= $from ?> to <?= $to ?></td>
        </tr>
        <tr>
            <td>Asset loans (Loans owed to your business)</td>
            <td>Increase</td>
            <td>Decrease</td>
        </tr>
        <?php
        $row = array(
            'Owner investment',
            'Asset',
            'Debt payment',
            'Transfer funds'
        );
        while (list($key) = each($row)) {
            ?>
            <tr>
                <td class="column-text-margin"><?= $row[$key] ?></td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["ali"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["ald"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
            </tr>

        <?php
        }
        ?>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Asset loans (Loans owed to your business)</td>
            <td colspan="2"></td>
        </tr>
        <?php
        $row = array(
            'Owner investment',
            'Asset',
            'Debt payment',
            'Transfer funds'
        );
        while (list($key) = each($row)) {
            ?>
            <tr>
                <td class="column-text-margin"><?= $row[$key] ?></td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["lli"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
                <td class="column-number">
                    <?php
                    $currValue = Helper::getIntegerValue($data["lld"][$row[$key]]);
                    echo Helper::formatNumber($currValue);
                    ?>
                </td>
            </tr>

        <?php
        }
        ?>
    </table>
    <br>
    <table class="table-bordered-report" width="100%"
        <?php
        if ($output != ReportFilterForm::VIEW_PDF) {
            echo ' border="1"';
        }
        ?>
        >

        <tr>
            <td colspan="2"><h3>Actual auto expense <span class="txt" style="color:red;font:italic 12px">(not adjusted for % business use of auto)</span>
                </h3></td>
        </tr>
        <tr>
            <td colspan="2" align="center">from <?= $from ?> to <?= $to ?></td>
        </tr>
        <tr>
            <td class="column-text-margin">Car and truck expenses</td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue(-$data["c"]["Car and truck expenses"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
        <tr>
            <td class="column-text-margin">Rent or lease - vehicles</td>
            <td class="column-number">
                <?php
                $currValue = Helper::getIntegerValue(-$data["c"]["Rent or lease - vehicles"]);
                echo Helper::formatNumber($currValue);
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td>
                <sup>1 </sup>VERY IMPORTANT: The IRS does not accept a lump sum total for Other Expense on your tax
                return. Use Custom Tags to create a secondary level of detail.
            </td>
        </tr>
        <?php
        if ($unknown != "") {
            ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <sup>* </sup>There are transactions in your account that have not been fully catagorized. Please
                    review and fully catagorize any <i>Unknown category - to be resolved</i> or <i>Imported Items</i>
                    transactions under in <b>Other business activity</b>.
                </td>
            </tr>
        <?php
        }
        ?>
    </table>
<?php
?>