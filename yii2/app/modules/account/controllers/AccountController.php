<?php
namespace app\modules\account\controllers;

use Yii;
use common\models\frontend\TransactionSearch;
use app\modules\account\components\Controller;

class AccountController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
