<?php
namespace app\modules\profile\controllers;

use Yii;
use common\components\Stripe;
use app\modules\profile\components\Controller;

/**
 * SubscriptionController
 */
class SubscriptionController extends Controller
{
    public function actionIndex()
    {
        $order = $this->findOrder();
        $customer = Stripe::getCustomer($order->stripe_customer);

        return $this->render('index', [
            'order' => $order,
            'package' => $order->getPackageObject(),
            'subscription' => $order->getSubscriptionObject(),
            'customer' => $customer
        ]);
    }

    public function actionCancelSubscription()
    {
        $order = $this->findOrder();
        if (Stripe::HasSubscription($order->stripe_customer)) {
            $order->cancelSubscription();
            if (Stripe::CancelSubscription($order->stripe_customer)) {
                $this->redirect('index');
            }
        }
    }
}
