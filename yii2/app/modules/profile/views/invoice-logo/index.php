<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<p>
    1.  Create a JPEG image of your invoice logo. The maximum dimensions for the image are 216 pixels wide by 70 pixels high. Please make sure that this image is in RGB color space. Files uploaded in CMYK color space are not browser-compatible.
</p>
<p>
    2. 	Save the image on your computer's hard drive. It is important that you remember where you saved the image.
</p>
<p>
    3. 	Use the "Browse" button below to locate the logo image you saved to your computer. Once you have found the image, click on the image file and click the 'Open' button on the dialog box.
</p>
<p>
    4. 	To insert your logo into the invoice format, click on the "Insert Logo" button.
</p>
<p>
    5. 	If you have previously uploaded a logo, the current logo is displayed below. Follow the instructions above and click on the "Insert Logo" button to replace the current logo. You can only have one invoice logo at a time per account.
</p>
<table width="40%">
    <?php
    if ($logo) {
        $img = $logo->getFileData()
            ->one()
            ->filedata;
        echo '<img src="data:image/jpeg;base64,'.base64_encode(stripslashes($img)).'" />';
    }
    ?>
    <tr>
        <td colspan="2">
        <?php
            $form = ActiveForm::begin([
            'options' =>[
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal'
            ],
            'fieldConfig' => [
                'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
            ]
            ]) ?>
            <?= $form->field($model,'imageFile')->fileInput() ?>
        </td></tr>
    <tr>
        <td>
            <?= Html::submitButton('Insert Logo', ['class' => 'btn btn-success','name' =>'insert']) ?>
            <?php ActiveForm::end() ?>
        </td>
        <td>
        <?php
            if ($logo) {
                 echo Html::beginForm(['remove']) . Html::submitButton('Remove Logo', [
                    'class' => 'btn btn-success',
                    'data-confirm' => 'Are you sure?'
                    ]) . Html::endForm();
             }
        ?>
        </td>
    </tr>
</table>