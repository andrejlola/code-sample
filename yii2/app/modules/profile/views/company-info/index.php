<?php

/* @var $this yii\web\View */
/* @var $order \common\models\backend\Order */

echo $this->render('_form', [
    'order' => $order,
]);
