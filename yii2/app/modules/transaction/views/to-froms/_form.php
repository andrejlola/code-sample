<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\backend\Category;
use common\models\frontend\CustomTag;
use common\models\backend\ZipCode;


/* @var $this yii\web\View */
/* @var $modelSource common\models\frontend\Source */
/* @var $modelAddress common\models\frontend\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>
<div class="model-content-wrapper">
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'data-pjax' => true,
            'id' => 'to-froms-form'
        ],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($modelSource, 'name')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelSource, 'active')
        ->defaultValue($modelSource::STATUS_ACTIVE)
        ->label('Status')
        ->radioList([
            'Y' => 'Active',
            'N' => 'Inactive',
        ], [
            'item' => function ($index, $label, $name, $checked, $value) use ($modelSource) {
                $id = Html::getInputId($modelSource, 'active') . '_' . $index;

                return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                    'id' => $id,
                    'value' => $value
                ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
            },
        ]) ?>

    <?= $form->field($modelSource, 'category_id')
        ->widget(Select2::classname(), [
            'data' => Category::getHierarchy(),
            'options' => [
                'placeholder' => 'Select A Category ...',
            ],
        ]) ?>

    <?= $form->field($modelSource, 'cust_tag_id')
        ->widget(Select2::classname(), [
            'data' => ArrayHelper::map(CustomTag::getAll(), 'id', 'name'),
            'options' => ['placeholder' => 'Select A Custom Tag ...'],

        ]) ?>

    <div class="more-model">
        <div class="plus-text">
            <i class="fa fa-plus"></i>
            Show more fields
        </div>
        <div class="minus-text">
            <i class="fa fa-minus"></i>
            Hide more fields
        </div>
        <div class="more">
            <?= $form->field($modelAddress, 'first_name')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'last_name')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'company')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'addr1')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'addr2')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'city')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'state')
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(ZipCode::getStates(true), 'id', 'state'),
                    'options' => ['placeholder' => 'Select a state ...'],

                ]) ?>

            <?= $form->field($modelAddress, 'zip')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'phone')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'fax')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'email')
                ->textInput(['maxlength' => true]) ?>

            <?= $form->field($modelAddress, 'foreign_addr')
                ->textarea(['maxlength' => true]) ?>
            <?= $form->field($modelAddress, 'country')
                ->hiddenInput(['value' => 'US'])
                ->label(false); ?>
        </div>
    </div>

    <div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span>' . ($modelSource->isNewRecord
                ? ' Add'
                : ' Update'), [
            'class' => 'btn btn-success'
        ]) ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php Pjax::end(); ?>
