<?php
namespace app\modules\transaction\controllers;

use Yii;
use common\models\frontend\BankAccount;
use app\modules\transaction\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class LoansController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BankAccount::find()
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => BankAccount::find()
                ->select([
                    '{{BANK_ACCOUNTS}}.*',
                    'sum(ifnull({{TRANSACTION}}.amount, 0)) as total_balance',
                    'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)>=0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as total_in',
                    'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)<0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as total_out'
                ])
                ->joinWith('transaction')
                ->where(['acct_type' => ['L','A']])
                ->groupBy('{{BANK_ACCOUNTS}}.id')
        ]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'totals' => BankAccount::getSumLoansTotal(true),
            'totalsType' => BankAccount::getSumLoansByTypeTotal(true),
        ]);
    }

    public function actionCreate()
    {
        $model = new BankAccount();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CustomTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BankAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
