<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\components\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $invoiceLineItem common\models\frontend\InvoiceLineItem */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $retURL string */

echo $this->render('_invoice_header',array('invoice' => $invoice));
?>
<div class="row text-center"><b>Invoice items</b></div>
<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnItemAdd" data-url="<?= Url::to([
                        'invoice/item',
                        'invoiceId' => $invoice->id,
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add
                    </button>
                    <a class="btn btn-success plus" href="<?= (isset($retURL) && !is_null($retURL))
                        ? $retURL
                        : Url::to('/invoice') ?>"><i
                            class="fa fa-arrow-left" aria-hidden="true"> </i> Return
                    </a>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
                <button title="Delete" data-action="delete" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action delete"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'invoice-lines-items-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'id' => 'invoice-lines',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Item',
                'attribute' => 'invoiceItem.name',
                'headerOptions' => ['width' => '100px;'],
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'attribute' => 'desc',
                'headerOptions' => ['width' => '200px;'],
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'label' => 'Qty',
                'value' => function ($invoice) {
                    return $invoice->qty;
                },
                'headerOptions' => ['width' => '50px;'],

            ],
            [
                'label' => 'Price',
                'attribute' => 'price_each',
                'headerOptions' => ['width' => '70px;'],
            ],
            [
                'label' => 'Total',
                'value' => function ($invoiceLineItem) {
                    return $invoiceLineItem->total_sum_no_tax;
                },
                'headerOptions' => ['width' => '70px;'],
                'contentOptions' => ['class' => 'text-right'],
                'footerOptions' => ['class' => 'text-right'],
                'footer' => $invoice->total_sum_without_tax

            ],

            [
                'class' => 'yii\grid\CheckboxColumn',
                'multiple' => false,
                'header' => 'Is tax',
                'headerOptions' => ['width' => '40px;'],
                'checkboxOptions' => function ($invoice, $key, $index, $column) {
                    $options = ['name' => 'tax', 'disabled' => 'disabled'];
                    if ($invoice->isTaxable()) {
                        $options['checked'] = 'checked';
                    }

                    return $options;
                },
            ],
            [
                'label' => 'Tax',
                'value' => function ($invoiceLineItem) {
                    return $invoiceLineItem->total_sum_tax;
                },
                'headerOptions' => ['width' => '60px;'],
                'contentOptions' => ['class' => 'text-right'],
                'footerOptions' => ['class' => 'text-right'],
                'footer' => $invoice->total_sum_tax

            ],
            [
                'label' => 'Total',
                'value' => function ($invoiceLineItem) {
                    return $invoiceLineItem->total_sum_with_tax;
                },
                'headerOptions' => ['width' => '70px;'],
                'contentOptions' => ['class' => 'text-right'],
                'footer' => $invoice->total_sum_with_tax,
                'footerOptions' => ['class' => 'text-right'],


            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'contentOptions' => ['style' => 'display:none;'],
                'buttons' => [
                    'update' => function ($url, $invoiceLineItem) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'data-action' => 'update',
                            'class' => 'btn btn-link',
                            'data-url' => Url::to([
                                'invoice/item',
                                'invoiceId' => $invoiceLineItem->invoice->id,
                                'id' => $invoiceLineItem->id,
                            ])
                        ]);
                    },
                    'delete' => function ($url, $invoice) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to([
                            'invoice/delete-invoice-line-item',
                            'id' => $invoice->id
                        ]), [
                            'data-action' => 'delete',
                            'data-pjax' => 'invoice-lines-items-pjax-grid',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modalInvoiceLineItem',
    'modalHeader' => 'Item',
    'selectorActivation' => '[data-action="update"],#btnItemAdd',
    'formId' => 'invoice-item-form',
    'gridPjaxId' => 'invoice-lines-items-pjax-grid',
]) ?>
