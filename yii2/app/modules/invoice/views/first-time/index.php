<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
?>
<p>
    Track Invoices is a simple tool for creating invoices and tracking each invoice that you create.
    This valuable tool is a stand alone feature and does not "talk to" or "interface with" any other module in <?php echo  Yii::$app->params['companyName']; ?>.
    Think of it as a way to create and print professional form invoices and keep track of what invoices are paid and unpaid - really simple that's the key.
</p>

<p>
    Creating an invoice is a two step process.
    <b>Create Invoice - Step 1</b> sets up the invoice by automatically generating an <b>Invoice Number</b> for you and you fill in the rest of the information
    - <b>Date</b> of invoice, <b>Customer</b>, <b>Purchase Order</b> (if any), <b>Terms</b> and <b>Due Date</b>.
</p>

<p>
    The <b>Last Five Invoices Created</b> are displayed on the screen for easy reference.
</p>

<p>
    Use the <b>Related Activities</b> on the right side of the screen to Create Invoices; to Pay, View and Edit Invoices; to Manage To/From's (Customers);
    to Manage Invoice Items; and to access the Invoice Aging Report.
</p>

<p>
    The <b>Invoice Aging</b> totals give you a quick reference for unpaid invoices by days aged.
</p>

<p align="center">
    <img src="/images/shots/invoicing-step-1-sized.jpg">
</p>

<p>
    <b>Create Invoice - Step 2</b> continues with data entry for the <b>Bill To</b> and <b>Ship To Addresses</b> and the <b>Itemized Sales and Credits</b>
    that you want to display on the invoice.  Itemizing your sales or credits is accomplished using <b>Invoice Items</b> which allow you to customize
    the names and descriptions you use for your sales and any credits that you may issue.
    Invoice Items can be preset so you can use the same item many times - see Manage Invoice Items in the Help Viewer.
</p>

<p>
    <b>Payments Received</b> from your customers are conveniently entered directly onto the matching invoice you have created which gives you a running history
    of the activity for each invoice.  This section will mostly likely not be used when you create an invoice unless you have received a down payment or deposit.
    The payments that you receive and enter on their matching invoice are <u>NOT recorded automatically</u> in the Enter Transaction module - what does this mean
    in plan English?  See Enter Payments Received in the Help Viewer for more details.
</p>

<p>
    The <b>Totals</b> box allows you to check your invoice totals both before printing your invoice and after you have received any payments.
    Here the <b>Sales Tax</b> is applied to the <b>Invoice Subtotal</b> and the <b>Payments Total</b> is considered to determine the current <b>Amount Due</b>
    for the invoice.  The <b>Sales Tax %</b> applied to the invoice subtotal is either (1) the <u>default</u> sales tax percentage you entered into your account
    Preferences or (2) the amount you enter directly into the Sales Tax % box on the invoice which overrides any default sales tax percentage you set in Preferences.
</p>

<p>
    The <b>Notes</b> box allows you to personalize your printed invoice with messages to your customers.
</p>

<p align="center">
    <img src="/images/shots/invoicing-step-2-sized.jpg">
</p>

<p>
    <b>Before you get started</b> creating invoices, we recommend that you set up the <b>Invoice Items</b> you know your business will be invoicing.
    Creating invoice items now will allow you to generate invoices more quickly in the future.
</p>

<p>
    After creating your invoice items, click on the <b>Create Invoices</b> link on the right side of the screen in <b>Related Activities</b>.
</p>

<p class="text-center">
    <strong><a href="<?= Url::to([$this->context->getHomeUrl()]) ?>">Take Me to Manage Invoice Items</a></strong>
</p>
