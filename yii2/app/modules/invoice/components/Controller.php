<?php
namespace app\modules\invoice\components;

use Yii;
use common\models\frontend\Invoice;
use common\components\Helper;
/**
 * Controller
 */
class Controller extends \app\components\Controller
{
    public $relatedLinks = [
        '/invoice' => 'Invoices',
        '/invoice/invoice-item' => 'Manage Invoice Items',
        '/invoice/first-time' => 'First Time In Help',
    ];

    public function getInfoWidget()
    {
        $data = Invoice::getInfoWidgetData();
        $result = [
            'header' => 'Invoices Aging',
            'items' => [
                'METER' => [
                    'items' => [
                        '0 - 30' => Helper::formatNumber($data["aging30"],''),
                        '31 - 60' => Helper::formatNumber($data["aging60"],''),
                        '61 - 90' => Helper::formatNumber($data["aging90"],''),
                        '91+' => Helper::formatNumber($data["aging90plus"],''),
                        'Total' => Helper::formatNumber($data["aging_total"],''),
                    ],
                ],
            ],
        ];

        return $result;
    }

}
