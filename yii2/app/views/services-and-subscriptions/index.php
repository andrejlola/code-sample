<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order common\models\backend\Order */
/* @var $package common\models\backend\Package */
/* @var $subscription common\models\backend\Subscription */
/* @var $customer \Stripe\Customer */

$this->title = 'My Services and Subscriptions';
?>

<div class="row">
    <div class="col-12">
        Package
    </div>
    <div class="col-12">
        <?= Html::encode($package->name) ?>
    </div>
</div>
<div class="row">
    <div class="col-2">
        Services
    </div>
    <div class="col-10">
        <?php foreach ($package->getServices()
            ->all() as $service
        ) { ?>
            <div class="row">
                <div class="col-10">
                    <?= Html::encode($service->name) ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-2">
        Subscription
    </div>
    <div class="col-10">
        <?= Html::encode($subscription->name) ?>
    </div>
</div>

<?php if (count($customer->subscriptions->all())) { ?>
    <div class="row">
        <div class="col-2">
            <?= Html::beginForm(['cancel-subscription']) . Html::submitButton('Cancel Subscription', [
                    'class' => 'btn btn-danger',
                    'data-confirm' => 'Are you sure you want to cancel active subscription'
                ]) . Html::endForm() ?>
        </div>
    </div>
<?php } ?>
