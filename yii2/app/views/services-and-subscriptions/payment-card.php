<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order common\models\backend\Order */
/* @var $customer \Stripe\Customer */

$this->title = 'My Payment Card';
?>

<div class="row">
    <div class="col-2">
        Payment Card
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>
                Card last 4 digits
            </th>
            <th>
                Expiration
            </th>
            <th>
            </th>
        </tr>
        </thead>
        <?php foreach ($customer->sources->data as $source) { ?>
            <tr>
                <td>
                    <?= Html::encode($source->last4) ?>
                </td>
                <td>
                    <?= Html::encode($source->exp_month) ?> / <?= Html::encode($source->exp_year) ?>
                </td>
                <td>
                    <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', [
                        '/services-and-subscriptions/payment-card-delete',
                        'id' => $source->id
                    ], [
                        'class' => 'btn btn-danger',
                        'title' => 'Delete Card'
                    ]) ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
